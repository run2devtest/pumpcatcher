from ccxt.base.errors import ExchangeError, RequestTimeout, ExchangeNotAvailable
from time import sleep, time
import sys
from datetime import datetime as dt

from exchangelink import ExchangeLink
from strategies import mfi
from strategies import ichimoku
from strategies import bollingerband

import pandas as pd
import atexit
import config

from bots import sendAlert, sendHaltAlert, sendStartAlert, sendSystemAlert
from model import recordAlert

import copy


EXCHANGE = 'bittrex'
PERIODS = ['1H', '4H', '1D']

color_change_set = set()
mfi_cross_set = set()
mfi_over_set = set()


def clear_alert_set():
    color_change_set.clear()


def check_colorchange(data, coin, timeframe):
    df = data.tail(2)
    close = data.tail(1)
    close = str(close['close'].values[0])
    colors = list(df['candle_color'])

    try:
        BBW = float(df['BB_WIDTH'].values[1])
        MFI = float(df['MFI'].values[1])

    except IndexError:
        BBW = 0
        MFI = 0

    if len(set(colors)) > 1:
        if colors[1] == 'green' or colors[1] == 'yellow':

            alert_name = str(coin + timeframe)

            if alert_name not in color_change_set:
                sendAlert(coin, timeframe, colors[0], colors[
                          1], EXCHANGE, close, BBW, MFI)

                recordAlert(coin, timeframe, colors[0], colors[
                    1], EXCHANGE, close, BBW, MFI)
            return alert_name


@atexit.register
def goodbye():
    sendHaltAlert(EXCHANGE)


def main(cycle_time):
    try:
        exchange = ExchangeLink(EXCHANGE)
        coin_pairs = exchange.get_exchange_coin_pairs()
        coin_length = len(coin_pairs)

        print('Found {} coins on {}'.format(coin_length, EXCHANGE))
        print(coin_pairs)

        cloud_pos = {'Below': [], 'Above': [],
                     'Inside': [], 'Unknown': []}

        cloud_position = dict([(p, copy.deepcopy(cloud_pos)) for p in PERIODS])
        timed_out = []

        for ticker in coin_pairs:
            try:

                print('Processing: {} Cycle Time: {:.2f}'.format(
                    ticker, cycle_time))
                data = exchange.get_historial_data(ticker)

                for period in PERIODS:

                    n_data = data[period]

                    ichi_data = ichimoku.Ichimoku(n_data)
                    n_data = ichi_data.get_ichimoku_data()
                    n_data = mfi.MFI(n_data).get_money_flow_data()
                    n_data = bollingerband.BB(n_data).get_bollinger_width()

                    color_change_set.add(
                        check_colorchange(n_data, ticker, period))

                    # ichimoku summary
                    position = ichi_data.get_price_cloud_relation()
                    cloud_position[period][position].append(ticker)
                sleep(exchange.get_rate_limit())

            except ExchangeError:
                pass

            except RequestTimeout:
                print(ticker, 'TimedOut.')
                timed_out.append(ticker)

            except KeyboardInterrupt:
                print('Exiting...')
                sys.exit()

        for cloud_summary in PERIODS:

            message = str(
                '{} {} Cloud Summary\n'.format(EXCHANGE.upper(), cloud_summary))
            message += str('Above cloud: ' +
                           str(cloud_position[cloud_summary]['Above']) + '\n')
            message += str('Inside cloud:' +
                           str(cloud_position[cloud_summary]['Inside']) + '\n')
            message += str('Below cloud:' +
                           str(cloud_position[cloud_summary]['Below']) + '\n')
            message += str('Processing Error: ' + str(cloud_position[cloud_summary][
                'Unknown']) + '\n')
            message += str('Timedout:' + str(timed_out) + '\n')
            message += str('GREEN/RED CHANGE: {}'.format(color_change_set) + '\n')
            message += str('MFI 50% CROSS: {}'.format(mfi_cross_set) + '\n')
            message += str('MFI OVER B/S: {}'.format(mfi_over_set) + '\n')
            print(message)

    except ExchangeNotAvailable:
        sendSystemAlert('{} reported unavailable.'.format(EXCHANGE))

if __name__ == '__main__':
    sendStartAlert(EXCHANGE)
    start_time = time()
    end_cycle_time = (time() - start_time) // 60
    while True:
        main(end_cycle_time)
        end_cycle_time = (time() - start_time) // 60
        if end_cycle_time >= config.getCycleTime():
            print('Restarting ALERTS/CYCLETIME')
            clear_alert_set()
            start_time = time()
        print('Cycle Time: {:.2f} Complete'.format(end_cycle_time))
        sleep(config.getCycleSleepTime())
