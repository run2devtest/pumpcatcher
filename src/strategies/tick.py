
class Tick:

    def __init__(self, raw_ohlc):
        self.date = raw_ohlc[0]
        self.open = raw_ohlc[1]
        self.high = raw_ohlc[2]
        self.low = raw_ohlc[3]
        self.close = raw_ohlc[4]
        self.volume = raw_ohlc[5]

    def __repr__(self):
        return 'Date: {}, Open: {}, High: {}, Low: {}, Close: {}, Volume: {}'.format(self.date,
                                                                                     self.open,
                                                                                     self.high,
                                                                                     self.low,
                                                                                     self.close,
                                                                                     self.volume)


class Ticker:

    def __init__(self, raw_ohlc_list):
        ticks = []
        for data in raw_ohlc_list:
            ticks.append(Tick(data))
        self.ticks = ticks
