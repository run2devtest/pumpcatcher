system_msg = """
--- FOMO DRIVEN DEVELOPMENT LOG ---

///25DEC17 CHANGES
Bot searches BTC volume instead of USD volume.
This should give more of an edge to finding 
where the money is flowing.

Added a twitter column. 
The twitr column will show a * when the coin is in 
the top 10 most mentioned coins in the last hour.

///26DEC17 CHANGES
Changed the algo a bit. Let me know if this is a little better. 
Should show more lower teir sleeper coins. 

///21JAN17 CHANGES
Added ichimoku and moneyflow alerts.
Eased up on the volume threshold and other settings.

///28JAN17 CHANGES
Made the some modifications to the thresholds. Will be a little bit more dynamic and strick.
FIXED ICHIMOKU SIGNALS. ADDED YELLOW CANDLES!!!!!!
RED TO GREEN SHOULD BE MUCH MORE ACCURATE!!!!!!!!!!!
Turned off MFI signals for testing.
"""

warning_msg = """

Coins listed by this bot do not equal buy siqnals.
You should be using this bot with your own TA to 
help you find good entry points."""

donate_msg = """

If you 100x your money using this bot.
Donate BTC 34TJtHXxp5bE5PNp57dHdnX6P8ZxP6Uc5F 

Please send feedback to 
Telegram: SOG31
Twitter: @THESOG31
Happy Trading.
"""


def getSystemMessage():
    return system_msg + warning_msg + donate_msg

if __name__ == '__main__':
    print(getSystemMessage())
