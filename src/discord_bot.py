from dhooks.discord_hooks import Webhook
from gettoken import GetToken
from time import sleep


def discord_exception(f):
    def wrapper(*args, **kw):
        try:
            return f(*args, **kw)

        except Exception as e:
            print(e, 'Error Detected')
            sleep(30)

    return wrapper


@discord_exception
def sendMessage(message, url):
    msg = Webhook(url, msg='```{}```'.format(message))
    msg.post()


@discord_exception
def sendHaltMessage(bot_name, url):
    message = '```{} BOT HALTED```'.format(bot_name)
    msg = Webhook(url, msg=message)
    msg.post()


@discord_exception
def sendStartMessage(bot_name, url):
    message = '```{} BOT STARTED```'.format(bot_name)
    msg = Webhook(url, msg=message)
    msg.post()


def test():
    url = GetToken('WEB_HOOK_TOKEN')
    sendMessage('test', url)
    sendStartMessage('test', url)
    sendHaltMessage('test', url)

if __name__ == '__main__':
    test()
