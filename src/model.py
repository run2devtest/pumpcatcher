from datetime import datetime
import pandas as pd
from peewee import *
from logger import GetLogger

log = GetLogger(__file__)

db = PostgresqlDatabase('pumpcatcher')


def getCurrentDateTime():
    return datetime.utcnow()


class BaseModel(Model):

    class Meta:
        database = db


class Alert(BaseModel):
    coin = CharField(null=True)
    pair = CharField(null=True)

    timeframe = CharField(null=True)

    color_one = CharField(null=True)
    color_two = CharField(null=True)

    exchange = CharField(null=True)

    price = DecimalField(max_digits=19, decimal_places=8, null=True)

    bollenger = DecimalField(max_digits=4, decimal_places=2, null=True)
    moneyflow = DecimalField(max_digits=4, decimal_places=2, null=True)

    created_date = DateTimeField()

    def addData(**kwargs):
        date = {'created_date': getCurrentDateTime()}
        updated_data = {**kwargs, **date}

        Alert.create(**updated_data)
        log.info('{} Alert added to database.'.format(
            (kwargs['coin'] + kwargs['pair']).upper()))

    def getCoinsNames():
        data = list(set([row.coin for row in Alert.select().distinct()]))
        return data

    def getRowsByName(coin):
        return list(Alert.select().where(Alert.coin == coin).dicts())

    def getAllRows():
        return list(Alert.select().dicts())


def recordAlert(coin, timeframe, color, color2, exchange, price, BBW, MFI):
    _coin = coin.split('/')[0]
    _pair = coin.split('/')[1]

    data = {'coin': _coin,
            'pair': _pair,
            'timeframe': timeframe,
            'color_one': color,
            'color_two': color2,
            'exchange': exchange,
            'price': price,
            'bollenger': BBW,
            'moneyflow': MFI}
    Alert.addData(**data)

    log_message = 'COIN: {} '
    log_message += 'TIMEFRAME: {} '
    log_message += 'COLORS: {} to {} '
    log_message += 'EXCHANGE: {} '
    log_message += 'PRICE: {} '
    log_message += 'BBW: {} '
    log_message += 'MFI: {} '
    log_message += 'RECORDED TO DB\n'

    log.info(log_message.format(coin, timeframe, color, color2,
                                exchange, price, BBW, MFI))


def getAllRowsDataFrame():
    columns = ['coin', 'pair', 'timeframe', 'color_one',
               'color_two', 'exchange', 'price', 'bollenger', 'moneyflow']
    return pd.DataFrame(Alert.getAllRows()).set_index('created_date')[columns]


def getRowsByNameDataFrame(coin):
    columns = ['coin', 'pair', 'timeframe', 'color_one',
               'color_two', 'exchange', 'price', 'bollenger', 'moneyflow']
    return pd.DataFrame(Alert.getRowsByName(coin.upper())).set_index('created_date')[columns]


def createTables():
    db.create_tables([Alert], True)
    log.info('Alert table create.')


def dropTables():
    db.drop_tables([Alert], True)
    log.info('Alert table dropped.')


def main():
    data = getAllRowsDataFrame()
    data.to_pickle('test_data.pickle')
    # print(getRowsByNameDataFrame('LtC'))
    pass
    # dropTables()
    # createTables()
    # recordAlert('TEST/TEST2', '4H', 'red', 'yellow',
    # 'bittrex', 0.00011784, 50.0, 20.0)

    # recordAlert('TESTx/TEST2', '1H', 'red', 'breen',
    # 'bittrex', 0.00011784, 50.0, 20.0)

if __name__ == '__main__':
    main()
