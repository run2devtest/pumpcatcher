def GetToken(name='TOKEN'):
    """Parses TOKEN file. Strips newlines and
    whitespaces. Returns token string.
    """
    with open(name, 'r') as TOKEN:
        return TOKEN.read().strip('\n').strip(' ')

if __name__ == '__main__':
    print(GetToken())
