import pandas as pd
import time


def getdata():
    url = 'https://coinmarketcap.com/all/views/all/'
    df = pd.read_html(url)[0]
    return df


def convertDollars(x):
    try:
        x = int(x.replace('$', '').replace(',', '').replace('.', ''))
        return x
    except ValueError:
        return 0


def convertPercent(x):
    try:
        x = float(x.replace('%', ''))
        return x
    except ValueError:
        return 0


def create_watchlist():
    df = getdata()

    df['Market Cap'] = df['Market Cap'].apply(convertDollars)
    df['Volume (24h)'] = df['Volume (24h)'].apply(convertDollars)
    df['Price'] = df['Price'].apply(convertDollars)

    df['% 1h'] = df['% 1h'].apply(convertPercent)
    df['% 24h'] = df['% 24h'].apply(convertPercent)
    df['% 7d'] = df['% 7d'].apply(convertPercent)

    df.sort_values(['% 1h'], ascending=True, inplace=True)

    columns = ['Symbol', 'Volume (24h)', '% 1h', '% 7d']

    df = df[df['Volume (24h)'] != 0]
    mean_volume = df['Volume (24h)'].mean()

    df = df[df['Volume (24h)'] > mean_volume]
    df = df[df['% 1h'] > 0]

    raw_string = 'Crypto Market Average Volume {} \n'.format(int(mean_volume))
    raw_string += '*' * 51 + '\n'

    raw_string += df[columns].to_string()
    return raw_string


if __name__ == '__main__':
    # starttime = time.time()
    # i = 30.0
    # while True:
    #     main()
    #     time.sleep(i - ((time.time() - starttime) % 30.0))
    print(create_watchlist())
