from coinmarketcap import Market
from logger import GetLogger

import pandas as pd
import pickle

from time import sleep
from twitter_mentions import get_top_mentions
from requests.exceptions import ConnectionError

pd.set_option('precision', 12)  # changes the pandas display percision

PICKLE_FILENAME = 'data.pickle'

log = GetLogger(__file__)


def filter_twitter(x, twitter_list):
    twitter_coins = twitter_list
    if x in twitter_coins:
        return '*'
    return ''


def get_coin_market_data():
    """
    returns coinmarketcap tickers

    """
    try:

        coinmarketcap = Market()

        stats = coinmarketcap.stats()
        currency_limit = stats['active_currencies'] + stats['active_assets']
        tickers = coinmarketcap.ticker(limit=currency_limit)

        if bool(tickers[0]['cached']):
            log.info('cached {} tickers from api.'.format(currency_limit))
        else:
            log.info('downloaded {} tickers from api.'.format(currency_limit))
        return tickers

    except Exception as e:
        log.info(e)
        return None


def clean_data(tickers):
    columns = ['rank',
               'name',
               'symbol',
               'id',
               '24h_volume_usd',
               'percent_change_1h',
               'percent_change_24h',
               'percent_change_7d',
               'available_supply',
               'price_btc']

    tickers = tickers

    global bitcoin, bitcoin_price
    bitcoin = tickers.pop(0)
    bitcoin_price = float(bitcoin['price_usd'])

    df = pd.DataFrame(tickers)

    # Drops useless data

    df.drop(['cached', 'last_updated', 'max_supply',
             'total_supply'], axis=1, inplace=True)

    df = df[df.id != 'tether']

    cols = df.columns.drop(['id', 'name', 'symbol'])
    df[cols] = df[cols].apply(pd.to_numeric, errors='coerce')
    df = df[columns]

    return df


def get_pumpers(dataframe, vol_multiplyer=16, percent_multiplyer_1h=2, percent_multiplyer_24h=2):
    df = dataframe

    df['24h_volume_btc'] = df['24h_volume_usd'] / bitcoin_price

    df.sort_values(['percent_change_1h'], ascending=True, inplace=True)
    df = df[df['24h_volume_usd'] > bitcoin_price]

    # print(df.tail(10))
    mean_volume_usd = df['24h_volume_usd'].mean()
    mean_volume_btc = df['24h_volume_btc'].mean()

    volume_threshold = mean_volume_usd / vol_multiplyer

    mean_1h_percent = df['percent_change_1h'].abs().mean()
    mean_24h_percent = df['percent_change_24h'].abs().mean()
    mean_7d_percent = df['percent_change_24h'].abs().mean()

    percent_threshold_1h = mean_1h_percent / percent_multiplyer_1h
    percent_threshold_24h = mean_24h_percent / percent_multiplyer_24h

    df = df[df['24h_volume_usd'] > volume_threshold]
    df = df[df['percent_change_1h'] > percent_threshold_1h]
    df = df[df['percent_change_24h'] > percent_threshold_24h]

    twitter_list = get_top_mentions()
    df['twitter_top'] = df['symbol'].apply(
        filter_twitter, args=(twitter_list,))

    columns = [
        'symbol',
        'percent_change_1h',
        'percent_change_24h',
        'percent_change_7d', 'twitter_top']

    log.info('Market Volume: ${:,.2f}'.format(mean_volume_usd))
    log.info('Volume Threshold: ${:,.2f}'.format(volume_threshold))
    log.info('1H Threshold: {}%'.format(percent_threshold_1h))
    log.info('24H Threshold: {}%'.format(percent_threshold_24h))
    log.info('DataFrame Lenght: {}'.format(len(df[columns])))

    package = {'dataframe': df[columns], 'mean_vol': mean_volume_usd, 'threshold_vol': volume_threshold,
               'threshold_1H': percent_threshold_1h, 'threshold_24H': percent_threshold_24h}

    return package


def createDataPickle(package):
    with open(PICKLE_FILENAME, 'wb') as file:
        pickle.dump(package, file)


def getDataPickle():
    with open(PICKLE_FILENAME, 'rb') as file:
        return pickle.load(file)


def parse(*args, **kwargs):
    try:

        tickers = get_coin_market_data()
        cleaned_data = clean_data(tickers)

        if kwargs:
            modified_data = get_pumpers(cleaned_data, vol_multiplyer=kwargs['vol_multiplyer'], percent_multiplyer_1h=kwargs[
                                        'percent_multiplyer_1h'], percent_multiplyer_24h=kwargs['percent_multiplyer_24h'])
        else:
            modified_data = get_pumpers(cleaned_data)

        createDataPickle(modified_data)
        data = getDataPickle()
        return data

    except ConnectionError as e:
        log.info(e)
        sleep(60)
        return getDataPickle()


if __name__ == '__main__':
    parse()
