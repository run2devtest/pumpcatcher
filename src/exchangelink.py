import ccxt
from ccxt.base.errors import RequestTimeout
import pandas as pd

from coin_mcap_parser import getDataPickle

pd.set_option('precision', 8)


class ExchangeLink():

    def __init__(self, exchange):

        if exchange:
            self.exchange = getattr(ccxt, exchange)({'enableRateLimit': True})

        else:
            log.debug("Unable to load exchange")

    def get_exchange_coin_pairs(self):
        try:
            exchange_pairs = [coin for coin in self.exchange.fetch_tickers(
            ).keys() if '/BTC' in coin or '/USDT' in coin]
            pumping_pairs = getPumpCatcherData()

            pairs = [x for x in pumping_pairs if x in exchange_pairs]
            return pairs
        except RequestTimeout:
            return []

    def get_historial_data(self, coin_pair):
        """
        Gets historical data from ccxt
        Returns OHLCV pandas dataframe
        """
        columns = ['date', 'open', 'high', 'low', 'close', 'volume']

        df = pd.DataFrame(self.exchange.fetch_ohlcv(
            coin_pair, timeframe='30m'), columns=columns)

        df['date'] = pd.to_datetime(df['date'], unit='ms')
        df.set_index('date', inplace=True)

        ohlc_dict = {'open': 'first', 'high': 'max',
                     'low': 'min', 'close': 'last', 'volume': 'sum'}

        columns.remove('date')

        df_30M = df[columns]
        df_1H = df.resample('1H').agg(ohlc_dict)[columns]
        df_4H = df.resample('4H').agg(ohlc_dict)[columns]
        df_1D = df.resample('1D').agg(ohlc_dict)[columns]

        return {'30M': df_30M, '1H': df_1H, '4H': df_4H, '1D': df_1D, 'ticker': coin_pair}

    def get_rate_limit(self):
        return self.exchange.rateLimit / 1000


def getPumpCatcherData():
    btc = [x + '/BTC' for x in list(getDataPickle()['dataframe']['symbol'])]
    usdt = [x + '/USDT' for x in list(getDataPickle()['dataframe']['symbol'])]
    return btc + usdt


def test():
    exchange = ExchangeLink('binance')
    data = exchange.get_historial_data('BTC/USDT')
    print(exchange.get_rate_limit())
    print(exchange.get_exchange_coin_pairs())


if __name__ == '__main__':
    test()
