import requests
import json
from bs4 import BeautifulSoup


def get_top_mentions():
    url = 'https://cointrendz.com/'

    resp = requests.get(url)
    soup = BeautifulSoup(resp.content, 'lxml')
    script = soup.find_all('script')[8]

    top_twitter = {}
    for i, line in enumerate(script.text.split('series:')[2].split('{')):
        if 'y:' in line:
            coin = str(line.split('name:')[1].split(',')[
                       0].replace("'", '')).split(' ')[1]
            mentions = int(line.split('y:')[1].split(',')[0])
            top_twitter[coin] = mentions
    return top_twitter

if __name__ == '__main__':
    print(get_top_mentions())
