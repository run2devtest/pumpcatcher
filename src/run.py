import coin_mcap_parser
import discord_bot
import telegram_bot
from gettoken import GetToken

import time

DISCORD_URL = GetToken('WEB_HOOK_TOKEN')

from logger import GetLogger

log = GetLogger(__file__)

lenght_list = []
vol_multiplyer = 2
percent_multiplyer_1h = 2
percent_multiplyer_24h = 2
last_mean_volume = None
last_threshold_volume = None

SLEEP_TIME = 2.5


def get_average_list_count():
    if lenght_list:
        return sum(lenght_list) / float(len(lenght_list))


def main():
    global vol_multiplyer, percent_multiplyer_1h, percent_multiplyer_24h, lenght_list, last_volume, last_mean_volume, last_threshold_volume

    if len(lenght_list) >= 2:
        log.info('Pass number {} recalculating thresholds'.format(len(lenght_list)))
        if get_average_list_count() > 15:

            if last_threshold_volume < last_mean_volume:
                log.info('increasing volume threshold')
                vol_multiplyer /= 1.5

            else:
                log.info('increasing percent threshold')
                percent_multiplyer_1h /= 1.5
                percent_multiplyer_24h /= 1.5

        elif get_average_list_count() < 5:
            if last_threshold_volume > 0:
                vol_multiplyer *= 2
                log.info('decreasing volume threshold')
            else:
                vol_multiplyer = 16
                log.info('setting default volume threshold')

        else:
            log.info('changing nothing')

        lenght_list = []

    data = coin_mcap_parser.parse(vol_multiplyer=vol_multiplyer,
                                  percent_multiplyer_1h=percent_multiplyer_1h, percent_multiplyer_24h=percent_multiplyer_24h)

    shortened_columns = {'24h_volume_usd': 'vol_usd', 'percent_change_1h': '1h_usd%',
                         'percent_change_24h': '24h_usd%',  'percent_change_7d': '7d_usd%', 'twitter_top': 'twitr'}

    df = data['dataframe']

    lenght_list.append(len(df))
    log.info('Pass {} starting... previous-passes{}'.format(
        len(lenght_list), lenght_list))

    log.info('current vol_multiplyer: {} percent_multiplyer_1h: {} percent_multiplyer_24h: {} last_mean_volume: {} last_threshold_volume: {}'.format(
        vol_multiplyer, percent_multiplyer_1h, percent_multiplyer_24h, last_mean_volume, last_threshold_volume))

    df.rename(index=str, columns=shortened_columns, inplace=True)
    mean_volume = data['mean_vol']
    threshold_volume = data['threshold_vol']

    last_mean_volume = mean_volume
    last_threshold_volume = threshold_volume

    raw_string = 'Crypto Market Avg Volume: ${:,} \n'.format(
        int(mean_volume))

    raw_string += 'Volume Threshold: ${:,} \n'.format(int(threshold_volume))

    raw_string += '1H Threshold: {:.2f}% \n'.format(data['threshold_1H'])

    raw_string += '24H Threshold: {:.2f}% \n'.format(data['threshold_24H'])

    title = 'SYMBOL    1H_US%  24H_US%  7D_US% TWITR'

    df_string = df.to_string(index=False).split('\n')
    df_string.pop(0)
    content = '\n'.join(df_string)

    raw_string += '*' * (len(title)) + '\n'
    raw_string += title + '\n'
    raw_string += content

    log.info(raw_string)

    telegram_bot.sendHTMLMessage(raw_string)
    discord_bot.sendMessage(raw_string, DISCORD_URL)

if __name__ == '__main__':
    while True:
        main()
        time.sleep(SLEEP_TIME * 60)
