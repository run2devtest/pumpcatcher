from speech_client import send_voice_msg


def message(pair, color, color2, period, exchange, price, bbw, mfi):
    if 'BTC' in pair:
        price = str(int(price * 100000000)) + " satoshi's"
    if 'USD' in pair:
        price = str(price) + ' dollars'
    if 'USDT' in pair:
        price = str(price) + ' tethers'
    periods = {'30M': 'Thirty Minute ', '1H': 'One Hour ',
               '4H': 'Four Hour ', '1D': 'Daily'}

    text = '{} {} {}, '.format(pair, exchange, periods[period])
    text += 'Changing {} to {},'.format(color, color2)

    send_voice_msg(text)

if __name__ == '__main__':
    message('SLS/BTC', 'red', 'yellow', '1D',
            'bittrex', 0.00651198, 5.53, 55.62)
