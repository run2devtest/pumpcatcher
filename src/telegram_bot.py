import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, Job
from telegram.error import (TelegramError, Unauthorized, BadRequest,
                            TimedOut, ChatMigrated, NetworkError)

from time import sleep

from gettoken import GetToken


def telegram_exception(f):
    def wrapper(*args, **kw):
        try:
            return f(*args, **kw)

        except Unauthorized as e:
            print(e, 'remove update. message.chat_id from conversation list')
            sleep(30)

        except BadRequest as e:
            print(e, 'handle malformed requests - read more below!')
            sleep(30)

        except TimedOut as e:
            print(e, 'handle slow connection problems')
            sleep(30)

        except NetworkError as e:
            print(e, 'handle other connection problems')
            sleep(30)

        except ChatMigrated as e:
            print(e, 'the chat_id of a group has changed, use e.new_chat_id instead')
            sleep(30)

        except TelegramError as e:
            print(e, 'handle all other telegram related errors')
            sleep(30)

        except Exception as e:
            print(e, 'Exception')
            sleep(30)

    return wrapper


def get_chat_channel():
    return '-1001125000839'


@telegram_exception
def sendHTMLMessage(message):
    bot = telegram.Bot(GetToken())
    message = '<pre>{}</pre>'.format(message)
    bot.send_message(chat_id=get_chat_channel(), text=message,
                     parse_mode=telegram.ParseMode.HTML)
    sleep(5)


@telegram_exception
def sendMessage(message, rest=True):
    bot = telegram.Bot(GetToken())
    bot.send_message(chat_id=get_chat_channel(), text=message)
    if rest:
        sleep(5)


@telegram_exception
def sendHaltMessage(bot_name):
    bot = telegram.Bot(GetToken())
    message = '{} BOT HALTED'.format(bot_name)
    bot.send_message(chat_id=get_chat_channel(), text=message)
    sleep(5)


@telegram_exception
def sendStartMessage(bot_name):
    bot = telegram.Bot(GetToken())
    message = '{} BOT STARTED'.format(bot_name)
    bot.send_message(chat_id=get_chat_channel(), text=message)
    sleep(5)


def test_bot():
    sendStartMessage('TESTING')
    sendHaltMessage('TESTING')
    sendHTMLMessage('TEST HTML FORMATED MESSAGE')
    sendMessage('TEST NORMAL FORMATED MESSAGE')


def bot_stress_test():
    while True:
        sendMessage('STRESS TEST', rest=False)

if __name__ == '__main__':
    test_bot()
