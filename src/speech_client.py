import os
from time import sleep

from sshtunnel import SSHTunnelForwarder, create_logger

import socket

HOST_ADDRESS = '67.205.159.81'
USERNAME = 'root'
PORT = 6666


def send_voice_msg(message):
    # create an ipv4 (AF_INET) socket object using the tcp protocol
    # (SOCK_STREAM)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # connect the client
    try:

        with SSHTunnelForwarder(
                HOST_ADDRESS,
                ssh_username=USERNAME,
                remote_bind_address=('0.0.0.0', PORT),
                logger=create_logger(loglevel=1)) as tunnel:

            client.connect(tunnel.local_bind_address)
            sleep(1)
            # send some data (in this case a HTTP GET request)
            client.send(message.encode())

            # receive the response data (4096 is recommended buffer size)
            response = client.recv(4096)

            if response.decode() == 'ACK!':
                print('message sent.')
            else:
                print('message not acknowledged.')

    except ConnectionRefusedError:
        print('meesage not sent.')
    except ConnectionResetError as e:
        print(e)

if __name__ == '__main__':
    send_voice_msg('test')
