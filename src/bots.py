import config
import discord_bot
import telegram_bot
import voice_alert

from datetime import datetime as dt
from gettoken import GetToken
from logger import GetLogger

from subprocess import DEVNULL, STDOUT, check_call

DISCORD_URL = GetToken('WEB_HOOK_TOKEN')
DISCORD_URL2 = GetToken('WEB_HOOK_TOKEN2')

log = GetLogger(__file__)


def sendStartAlert(exchange):
    msg = ('{} PUMPER'.format(exchange.upper()))
    discord_bot.sendStartMessage(msg, DISCORD_URL)
    telegram_bot.sendStartMessage(msg)


def sendHaltAlert(exchange):
    msg = ('{} PUMPER'.format(exchange.upper()))
    discord_bot.sendHaltMessage(msg, DISCORD_URL)
    telegram_bot.sendHaltMessage(msg)


def sendSystemAlert(message):
    discord_bot.sendMessage(message, DISCORD_URL)
    telegram_bot.sendHTMLMessage(message)


def sendAlert(coin, timeframe, color, color2, exchange, close, BBW, MFI):
    time = dt.now().strftime("%I:%M:%S")  # used for current time on message

    message = ('{} {} \nChanging from {} to {} \nPrice:{:.8f} Time: {} \nExchange: {}\nBollinger Width: {:.2f}\nMFI: {:.2f}'.format(
        coin, timeframe, color.title(), color2.title(), float(close), time, exchange.title(), float(BBW), float(MFI)))

    log.info('sendAlert: {}'.format(message.replace('\n', ' ')))

    discord_bot.sendMessage(message, DISCORD_URL)
    discord_bot.sendMessage(message, DISCORD_URL2)
    telegram_bot.sendHTMLMessage(message)
    voice_alert.message(coin, color, color2, timeframe,
                        exchange, float(close), BBW, MFI)


def playSound(sound='sound.wav'):
    SOUND = config.getSoundSetting()
    if SOUND:
        check_call(['/usr/bin/aplay', sound],
                   stdout=DEVNULL, stderr=STDOUT)


if __name__ == '__main__':
    sendSystemAlert('Test System Message')
    sendStartAlert('test')
    sendHaltAlert('test')
    sendAlert('TEST/TEST2', '4H', 'red', 'yellow',
              'bittrex', 0.00011784, 50.0, 20.0)
